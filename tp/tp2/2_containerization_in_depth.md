# TP2 Containerization in-depth

Ce deuxième TP a pour objectif d'aller voir un peu plus en profondeur comment fonctionne la conteneurisation sous GNU/Linux, en utilisant notamment Docker pour l'appréhender.  

N'hésitez pas à jouer avec les commandes et concepts présentés dans ce TP, et ne pas vous limiter à ce qui est demandé de faire :)

- [TP2 Containerization in-depth](#tp2-containerization-in-depth)
- [I. Gestion de conteneurs Docker](#i-gestion-de-conteneurs-docker)
- [II. Sandboxing](#ii-sandboxing)
  - [1. Namespaces](#1-namespaces)
    - [A. Exploration manuelle](#a-exploration-manuelle)
    - [B. `unshare`](#b-unshare)
    - [C. Avec docker](#c-avec-docker)
    - [D. `nsenter`](#d-nsenter)
    - [E. Et alors, les namespaces User ?](#e-et-alors-les-namespaces-user)
    - [F. Isolation réseau ?](#f-isolation-r%c3%a9seau)
  - [2. Cgroups](#2-cgroups)
    - [A. Découverte manuelle](#a-d%c3%a9couverte-manuelle)
    - [B. Utilisation par Docker](#b-utilisation-par-docker)
  - [3. Capabilities](#3-capabilities)
    - [A. Découverte manuelle](#a-d%c3%a9couverte-manuelle-1)
    - [B. Utilisation par Docker](#b-utilisation-par-docker-1)
- [III. Conteneurs avec d'autres outils](#iii-conteneurs-avec-dautres-outils)

# I. Gestion de conteneurs Docker

> 📚 [**Cours associé (sa lecture est un pré-requis).**](../../cours/containers_in_depth.md#1-gestion-de-conteneurs-par-docker)

Il est nécessaire d'avoir Docker de lancé pour la suite.

* 🌞 Mettre en évidence l'utilisation de chacun des processus liés à Docker
  * `dockerd`, `containerd`, `containerd-shim`
  * analyser qui est le père de qui (en terme de processus, avec leurs PIDs)
  * avec la commande `ps` par exemple

---

* 🌞 Utiliser l'API HTTP mise à disposition par `dockerd`
  * utiliser un `curl` (ou autre) pour discuter à travers le socker UNIX
  * la [documentation de l'API est dispo en ligne](https://docs.docker.com/engine/api/v1.40/)
  * récupérer la liste des conteneurs
  * récupérer la liste des images disponibles

# II. Sandboxing

Le lancement d'un conteneur se fait dans un environnement sandboxé. Autrement dit : 
* il est isolé (vision restreinte des éléments du système, comme les processus ou les utilisateurs)
* il a des restrictions d'accès aux ressources matérielles
* il a des droits restreints
* il est surveillé/monitoré

C'est en grande partie grâce aux fonctionnalités que nous allons voir dans cette partie que ces mesures de sécurité sont en mises en place :
* namespaces
* cgroups
* capabilities
* autres

## 1. Namespaces

> 📚 [**Cours associé (sa lecture est un pré-requis).**](../../cours/containers_in_depth.md#a-namespaces)

### A. Exploration manuelle

Les namespaces sont identifiés par un ID de 10 digits. On peut trouver les namespaces d'un processus donné en utilisant le pseudo-filesystem `/proc`. Plus précisément, on peut les trouver dans le répertoire dédié à ce processus : `/proc/PID`.

```
$ ls -al /proc/<PID>/ns
```

> `/proc` et `/sys` sont des API sur le kernel. Elles sont présentées sous la forme de filesystems pour pouvoir être utilisées simplement avec les outils natifs de GNU/Linux (philosophie "tout est fichier").

🌞 Trouver les namespaces utilisés par votre shell. 

> On peut aussi lister l'ensemble des namespaces qui existent sur la machine avec `lsns`

### B. `unshare`

On peut créer des namespaces avec l'appel système `unshare()`. Il existe une commande `unshare` qui nous permet de l'utiliser directement.  

🌞 Créer un pseudo-conteneur à la main en utilisant `unshare`
* lancer une commande `unshare`
* `unshare` doit exécuter le processus `bash`
* ce processus doit utiliser des namespaces différents de votre hôte :
  * réseau
  * mount
  * PID
  * user
* prouver depuis votre `bash` isolé que ces namespaces sont bien mis en place

### C. Avec docker

Lancer un conteneur qui tourne en tâche de fond, sur un sleep :
```
$ docker run -d debian sleep 99999
```

🌞 Trouver dans quels namespaces ce conteneur s'exécute.

### D. `nsenter`

Il existe une commande qui permet d'exécuter des processus dans un namespace donné : `nsenter`.  

🌞 Utiliser `nsenter` pour rentrer dans les namespaces de votre conteneur en y exécutant un shell
* prouver que vous êtes isolé en terme de réseau, arborescence de processus, points de montage

### E. Et alors, les namespaces User ?

L'implémentation des namespaces de type User dans le noyau a mis plus de temps que les autres namespaces.  
Son fonctionnement est aussi radicalement différent des autres, avec chaque utilisateur d'un nouveau namespace qui est mappé vers un utilisateur du namespace parent, afin de garder une gestion de droit cohérente dans le namespace parent.  

Il est cependant possible de l'utiliser, et de demander à Docker de le mettre à profit.  

🌞 Mettez en place la configuration nécessaire pour que Docker utilise les namespaces de type User.

### F. Isolation réseau ? 

> 📚 [**Cours associé (sa lecture est un pré-requis).**](../../cours/containers_in_depth.md#isolation-réseau-)

Observer les opérations liées au réseau lors de l'exécution d'un conteneur
* 🌞 lancer un conteneur simple
  * je vous conseille une image `debian` histoire d'avoir des commandes comme `ip a` qui n'existent pas dans une image allégée comme `alpine`)
  * ajouter une option pour partager un port (n'importe lequel), pour voir plus d'informations
    * `docker run -d -p 8888:7777 debian sleep 99999`
* 🌞 vérifier le réseau du conteneur
  * vérifier que le conteneur a bien une carte réseau et repérer son IP
    * c'est une des interfaces de la *veth pair*
  * possible avec un shell dans le conteneur OU avec un `docker inspect` depuis l'hôte
* 🌞 vérifier le réseau sur l'hôte
  * vérifier qu'il existe une première carte réseau qui porte une IP dans le même réseau que le conteneur
  * vérifier qu'il existe une deuxième carte réseau, qui est la deuxième interface de la *veth pair*
    * son nom ressemble à *vethXXXXX@ifXX*
  * identifier les règles *iptables* liées à la création de votre conteneur  



## 2. Cgroups

### A. Découverte manuelle

> 📚 [**Cours associé (sa lecture est un pré-requis).**](../../cours/containers_in_depth.md#b-cgroups)

On peut voir les *cgroups utilisés* par un processus donné dans `/proc`

```
$ cat /proc/<PID>/cgroup
```

🌞 Lancer un conteneur Docker et déduire dans quel cgroup il s'exécute

> Sur un système muni de systemd, on peut utiliser la commande `systemd-cgtop` et `systemd-clgs` pour avoir un aperçu du monitoring effectué par les cgroups. Ces deux commandes vont lire dans le répertoire `/sys/fs/cgroup` afin d'afficher des informations sur les CGroups, sous un format un peu plus human-readable.

---

Les cgroups permettent de grouper des processus et limiter l'accès aux ressources auxquelles a accès l'OS, comme la RAM, les I/O disque, le réseau, etc.  
Pour observer les limites mises en place, il faut utiliser un deuxième pseudo-filesystem : `sys` ; plus précisément en explorant `/sys/fs/cgroup`.

* le répertoire `/sys/fs/cgroup` représente la structure arborescente des cgroups
* ainsi le réperoire `/sys/fs/cgroup/memory` contient les informations relatives aux cgroups concernant la RAM
* on peut trouver un sous-répertoire `/sys/fs/cgroup/memory/docker` qui contient des informations relatives aux cgroups liés à Docker

### B. Utilisation par Docker

🌞 Lancer un conteneur Docker et trouver 
* la mémoire RAM max qui lui est autorisée
* le nombre de processus qu'il peut contenir
* explorer un peu de vous-même ce qu'il est possible de faire avec des cgroups  

🌞 Altérer les valeurs cgroups allouées par défaut avec des options de la commandes `docker run` (au moins 3)
* préciser les options utilisées
* prouver en regardant dans `/sys` qu'elles sont utilisées

## 3. Capabilities

### A. Découverte manuelle

> 📚 [**Cours associé (sa lecture est un pré-requis).**](../../cours/containers_in_depth.md#c-capabilities)

On peut lister les capabilities utilisées par les processus du système avec la commande `pscap`.  

Il est aussi possible d'interagir avec les capabilities avec `capsh`. 

Utiliser `capsh --print`
* cela affiche des informations avancées sur votre shell
* 🌞 déterminer les capabilities actuellement utilisées par votre shell

---

On peut aussi obtenir des informations avec `/proc`. Pour voir les capabilities d'un processus donné :
```
$ cat /proc/<PID>/status | grep Cap

# Les valeurs obtenues sont en hexadécimal. On peut utiliser capsh pour les décoder
$ capsh --decode 0000003fffffffff
```

🌞 Déterminer les capabilities du processus lancé par un conteneur Docker
* utiliser quelque chose de simple pour le conteneur comme un `docker run -d alpine sleep 99999`
* en utilisant `/proc`

---

Les fichiers exécutables ont aussi un jeu de capabilities, qui définissent les capabilities qu'ils pourront acquérir une fois lancé.  

On peut voir les capabilities d'un exécutable avec `getcap <FILE>` et les définir avec `setcap <CAPABILITIES> <FILE>`.

🌞 Jouer avec `ping`
* trouver le chemin absolu de `ping`
* récupérer la liste de ses capabilities
* enlever toutes les capabilities
  * en utilisant une liste vide
  * `setcap '' <PATH>` 
* vérifier que `ping` ne fonctionne plus
* vérifier avec `strace` que c'est bien l'accès à l'ICMP qui a été enlevé
  * **NB** : vous devrez aussi ajouter des capa à `strace` pour que son `ping` puisse en hériter !

> N'oubliez pas de remettre ses capabilities à votre `ping` ;)

### B. Utilisation par Docker

Docker fait usage des capabilities nativement. Le `root` d'un conteneur est donc moins puissant que le `root` réel de la machine.  

On peut choisir les capabilities qu'un conteneur sera autorisé à utiliser avec des options de `docker run`.
* `docker run --cap-drop` pour enlever des capabilities
* `docker run --cap-add` pour ajouter des capabilities
* on peut effectuer un whitelisting des capabilities, en les enlevant toutes puis en n'autorisant uniquement celles précisées avec :  
`docker run --cap-drop all --cap-add <CAPA> --cap-add <CAPA>`  

🌞 lancer un conteneur NGINX qui a le strict nécessaire de capabilities pour fonctionner
* prouver qu'il fonctionne
* expliquer toutes les capabilities dont il a besoin

# III. Conteneurs avec d'autres outils

Un conteneur n'est qu'un concept :
* embarque une application et son environnement
* environnement d'exécution isolé

Le concept de conteneurs peut donc se présenter sous plusieurs formes :
* comme on l'a vu en II. : avec des mécanismes kernel sur un système GNU/Linux
* sur Windows en utilisant des technologies spécifiques
* ou même sous forme de machines virtuelles (on va y venir)

Ainsi, nous allons voir dans cette partie d'autres outils pour mettre en place la conteneurisation.

Il est recommandé de lire [la partie du cours liée à la standardisation autour des la conteneurisation](../cours/../../cours/containers_in_depth.md#ii-standardisation) afin de mieux saisir ce qui va suivre.

**EDIT** : on va au final passer à la suite afin de profiter au mieux du temps qu'il nous reste !
