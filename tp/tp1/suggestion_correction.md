# Prise en main de Docker

Ce TP a pour but de prendre en main l'outil `docker`, sans rentrer dans les détails de son implémentation ou de son écosystème.  

L'objectif est de vous rendre autonome sur le lancement et la création de services conteneurisés. 

- [Prise en main de Docker](#prise-en-main-de-docker)
- [0. Setup](#0-setup)
- [I. Prise en main](#i-prise-en-main)
  - [1. Lancer des conteneurs](#1-lancer-des-conteneurs)
  - [2. Gestion d'images](#2-gestion-dimages)
    - [Gestion d'images](#gestion-dimages)
    - [Création d'image](#cr%c3%a9ation-dimage)
  - [3. Manipulation du démon docker](#3-manipulation-du-d%c3%a9mon-docker)
- [II. `docker-compose`](#ii-docker-compose)
  - [Basics](#basics)
  - [Write your own](#write-your-own)
  - [Rangement](#rangement)
  - [Conteneuriser une application donnée](#conteneuriser-une-application-donn%c3%a9e)

# 0. Setup

```
# Disable SELinux
[it4@tp1 ~]$ sudo setenforce 0
[it4@tp1 ~]$ sudo sed 's/enforcing/permissive/g' /etc/selinux/config

# Enable masquerading (for docker port forwarding)
[it4@tp1 ~]$ sudo firewall-cmd --add-masquerade --permanent
success
[it4@tp1 ~]$ sudo firewall-cmd --reload
success

# From official docs
[it4@tp1 ~]$ sudo yum install -y yum-utils device-mapper-persistent-data lvm2
[it4@tp1 ~]$ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
[it4@tp1 ~]$ sudo yum install -y docker-ce --nobestA

# Ajouter le user courant au groupe docker
[it4@tp1 ~]$ sudo usermod -aG docker $(whoami)

# Démarrer docker et activer au boot
[it4@tp1 ~]$ sudo systemctl enable --now docker

# Test
[it4@tp1 ~]$ docker info
```

# I. Prise en main

## 1. Lancer des conteneurs

```
[it4@tp1 ~]$ docker run -d alpine sleep 9999
a8695eb4a05d55623653e8abbfb7f746b98957cd30fa06f0356dfbd1c9ba4c48

[it4@tp1 ~]$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
a8695eb4a05d        alpine              "sleep 9999"        2 seconds ago       Up 1 second                             heuristic_hertz
```

`docker ps` ne retourne qu'un seul conteneur actif, c'est celui que nous venons de lancer. L'ID listé (avec `docker ps`) correspond à l'ID fournit lors du lancement (avec `docker run`).  
Son nom est `heuristic_hertz`.

---

Pour mettre en évidence l'isolation du conteneur par rapport à l'hôte, on va regarder l'état du système vu depuis le conteneur et vu depuis l'hôte. Commandes utilisées :
* `ps -ef` pour lister les processus
* `ip a` pour voir les cartes réseau
* `cat /etc/passwd` pour voir les utilisateurs disponibles
* `df -h` pour voir les points de montage (stockage)

**Ce qui est vu par le conteneur :** 
```
# On exécute un shell dans le conteneur et on se place dedans
[it4@tp1 ~]$ docker exec -it heuristic_hertz sh

# Liste des processus
/ # ps -ef
PID   USER     TIME  COMMAND
    1 root      0:00 sleep 9999
   10 root      0:00 sh
   14 root      0:00 ps -ef

# Liste des cartes réseau
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
5: eth0@if6: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever

# Liste des utilisateurs
/ # cat /etc/passwd
root:x:0:0:root:/root:/bin/ash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/mail:/sbin/nologin
news:x:9:13:news:/usr/lib/news:/sbin/nologin
uucp:x:10:14:uucp:/var/spool/uucppublic:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
man:x:13:15:man:/usr/man:/sbin/nologin
postmaster:x:14:12:postmaster:/var/mail:/sbin/nologin
cron:x:16:16:cron:/var/spool/cron:/sbin/nologin
ftp:x:21:21::/var/lib/ftp:/sbin/nologin
sshd:x:22:22:sshd:/dev/null:/sbin/nologin
at:x:25:25:at:/var/spool/cron/atjobs:/sbin/nologin
squid:x:31:31:Squid:/var/cache/squid:/sbin/nologin
xfs:x:33:33:X Font Server:/etc/X11/fs:/sbin/nologin
games:x:35:35:games:/usr/games:/sbin/nologin
cyrus:x:85:12::/usr/cyrus:/sbin/nologin
vpopmail:x:89:89::/var/vpopmail:/sbin/nologin
ntp:x:123:123:NTP:/var/empty:/sbin/nologin
smmsp:x:209:209:smmsp:/var/spool/mqueue:/sbin/nologin
guest:x:405:100:guest:/dev/null:/sbin/nologin
nobody:x:65534:65534:nobody:/:/sbin/nologin

# Liste des points de montage
/ # df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                   6.2G      1.7G      4.5G  27% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                   410.7M         0    410.7M   0% /sys/fs/cgroup
/dev/mapper/cl-root       6.2G      1.7G      4.5G  27% /etc/resolv.conf
/dev/mapper/cl-root       6.2G      1.7G      4.5G  27% /etc/hostname
/dev/mapper/cl-root       6.2G      1.7G      4.5G  27% /etc/hosts
shm                      64.0M         0     64.0M   0% /dev/shm
tmpfs                   410.7M         0    410.7M   0% /proc/asound
tmpfs                   410.7M         0    410.7M   0% /proc/acpi
tmpfs                    64.0M         0     64.0M   0% /proc/kcore
tmpfs                    64.0M         0     64.0M   0% /proc/keys
tmpfs                    64.0M         0     64.0M   0% /proc/timer_list
tmpfs                    64.0M         0     64.0M   0% /proc/sched_debug
tmpfs                   410.7M         0    410.7M   0% /proc/scsi
tmpfs                   410.7M         0    410.7M   0% /sys/firmware
```

**Ce qui est vu par l'hôte :**
```
# Liste des processus
[it4@tp1 ~]$ ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 06:06 ?        00:00:01 /usr/lib/systemd/systemd --switched-root --system --deserialize 18
[...]
it4      11209 11208  0 06:16 pts/0    00:00:00 -bash
root     11264 11061  0 06:17 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/docker/containerd/daemon/io.containerd.runtime.v1.linux/moby/a8695eb4a05d55623653e8abbfb7f746b98957cd30fa06f0356dfbd1c9ba4c48 -ad
root     11279 11264  0 06:17 ?        00:00:00 sleep 9999
it4      11459 11209  0 06:25 pts/0    00:00:00 ps -ef

# Liste des cartes réseau
[it4@tp1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b8:86:f8 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85256sec preferred_lft 85256sec
    inet6 fe80::4458:9ba3:29c2:e596/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ae:0e:07 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.6/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 655sec preferred_lft 655sec
    inet6 fe80::528:a50b:6c4a:ffd2/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:0f:e3:c7:8b brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:fff:fee3:c78b/64 scope link 
       valid_lft forever preferred_lft forever
6: veth46717d5@if5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether d6:7a:59:47:0c:50 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::d47a:59ff:fe47:c50/64 scope link 
       valid_lft forever preferred_lft forever

# Liste des utilisateurs
[it4@tp1 ~]$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
[...]
it4:x:1000:1000:it4:/home/it4:/bin/bash

# Liste des points de montage
[it4@tp1 ~]$ df -h
Filesystem           Size  Used Avail Use% Mounted on
devtmpfs             396M     0  396M   0% /dev
tmpfs                411M     0  411M   0% /dev/shm
tmpfs                411M  5.7M  406M   2% /run
tmpfs                411M     0  411M   0% /sys/fs/cgroup
/dev/mapper/cl-root  6.2G  1.7G  4.6G  27% /
/dev/sda1            976M  127M  782M  14% /boot
tmpfs                 83M     0   83M   0% /run/user/1000
```

On constate effectivement que le conteneur voit des ressources différentes par rapport à l'hôte. 

Destruction du conteneur :
```
[it4@tp1 ~]$ docker rm -f heuristic_hertz
```

---

Dans la VM, lancement du NGINX container :
```
[it4@tp1 ~]$ docker run -d -p 8080:80 nginx
d7c26bd450f604e9c09776150757f7ba55129b543b43340faa42df07090a67be

# Test en local
[it4@tp1 ~]$ curl localhost:8080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
[...]
</html>
```

Consultation depuis la machine hôte :
```
# Sur la VM
[it4@tp1 ~]$ sudo firewall-cmd --add-port=8080/tcp
success

# Depuis l'hôte
[it4@nowhere tp1]$ curl 10.2.1.6:8080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
[...]
</html>
```

## 2. Gestion d'images

Apache container : 
```
# Récupération de l'image Apache en version 2.2
[it4@tp1 ~]$ docker pull httpd:2.2

# Lancement avec partage de port 
[it4@tp1 ~]$ docker run -d -p 8080:80 httpd:2.2
541cc2430205e086b6b2b50aea713f919fd246d03179e6b2faac91adec720417

# Test
[it4@tp1 ~]$ curl localhost:8080
<html><body><h1>It works!</h1></body></html>
```

### Création d'image

Le Dockerfile résultant en un serveur web Python simple [est visible **ici**](./I.2./Dockerfile). 

```
# Build de l'image (transformation du Dockerfile en une image Docker)
# A exécuter en étant dans le répertoire contenant le Dockerfile
[it4@tp1 1]$ docker build . -t python-webserver

# Run du conteneur
[it4@tp1 1]$ docker run -d -p 7777:8888 python-webserver
a96dc088bc2aa681f511804fd64f16c22b0955ce6b3b5309f4b3a3ed5e1f19cf
[it4@tp1 1]$ curl localhost:7777
<h1>Hello bro</h1>

# Run du conteneur avec un volume
[it4@tp1 I.2.]$ docker run -d -p 7777:8888 -v /home/it4/I.2./files:/app python-webserver
4d397ebfcb3d19042cf66b940e742cf1e725fdbfc07f490661abf0fcbed2fe0c
[it4@tp1 I.2.]$ curl localhost:7777
<h1>Hello bro</h1>
```

## 3. Manipulation du démon docker

Afin de modifier la configuration Docker, il est possible de modifier deux fichiers (l'un OU l'autre) :
* le fichier de configuration JSON, lu par défaut
  * `/etc/docker/daemon.json`
* directement la ligne de commande qui lance `dockerd`
  * définie dans `/etc/systemd/system/docker.service`
  * ce chemin est donné avec un `systemctl status docker`

---

Dans l'unité de service :
```
[it4@tp1 ~]$ sudo vim /etc/systemd/system/docker.service
# on remplace la ligne
ExecStart=/usr/bin/dockerd -H fd:// 

# par la ligne
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:6666 --data-root /data --oom-score-adjust 200

# on redémarre Docker
[it4@tp1 ~]$ sudo systemctl daemon-reload # on indique à l'OS (à systemd) que l'on a modifié l'unité de service
[it4@tp1 ~]$ sudo systemctl restart docker 
```

---

**OU** on peut utiliser le fichier de configuration :
```
[it4@tp1 ~]$ sudo vim /etc/systemd/system/docker.service
# on remplace la ligne
ExecStart=/usr/bin/dockerd -H fd:// 

# par la ligne
#ExecStart=/usr/bin/dockerd -H fd:// # commentée

# modif d'une unité de service
[it4@tp1 ~]$ sudo systemctl daemon-reload # on indique à l'OS (à systemd) que l'on a modifié l'unité de service

# puis on modifie le fichier de conf comme suit 
[it4@tp1 ~]$ sudo cat /etc/docker/daemon.json 
{
  "hosts": ["fd://","unix:///var/run/docker.sock","tcp://0.0.0.0:2376"],
  "oom-score-adjust": -200,
  "data-root": "/opt/docker"
}

# redémarrage de docker
[it4@tp1 ~]$ sudo systemctl restart docker 
```

Test : 
```
# Vérification du port TCP (2376 est le port configuré précédemment)
[it4@tp1 ~]$ sudo ss -alnpt | grep 2376
LISTEN   0         128                       *:2376                   *:*        users:(("dockerd",pid=8032,fd=7))  
# Utilisation de Docker via ce port TCP
[it4@tp1 ~]$ docker -H 127.0.0.1:2376 ps

# Vérification du data root
[it4@tp1 ~]$ docker info | grep Root
 Docker Root Dir: /opt/docker
[it4@tp1 ~]$ sudo ls /opt/docker/
builder  buildkit  containers  image  network  overlay2  plugins  runtimes  swarm  tmp	trust  volumes

# Vérification du OOM_score
[it4@tp1 ~]$ systemctl status docker | grep PID
 Main PID: 11615 (dockerd)
[it4@tp1 ~]$ sudo cat /proc/11615/oom_score_adj 
-200
```

# II. `docker-compose`

`docker-compose` est un outil utilisé pour lancer plusieurs conteneurs qui fonctionnent de concert.  

Il remplace l'utilisation de la commande `docker run` en permettant de stocker toutes les informations dans un unique fichier au format YML et propose des fonctionnalités supplémentaires pour la gestion de plusieurs conteneurs.

Pour continuer il faudra effectuer l'[installation de `docker-compose` en suivant la doc officielle.](https://docs.docker.com/compose/install/)

## Basics

Créer un répertoire de travail qui contient le fichier `docker-compose.yml` avec le contenu qui suit :

```
version: "3.3"

services:
  server:
    image: nginx
    networks:
      nginx-net:
        aliases:
          - nginx.test
          - nginx
    ports:
      - "8080:80"

networks:
  nginx-net:
```

Il est possible d'interagir avec en utilisant les commandes : 
```
# Lancer les conteneurs
$ docker-compose up

# Couper les conteneurs
$ docker-compose down

# Demander le build de tous les conteneurs si un Dockerfile est renseigné 
$ docker-compose build

# Obtenir des infos sur les conteneurs qui tournent
$ docker-compose ps
$ docker-compose top
$ docker stats
```

## Write your own

🌞 Ecrire un `docker-compose-v1.yml` qui permet de :
* lancer votre image de serveur web Python créée en [2.](#cr%c3%a9ation-dimage)
* partage le port TCP du conteneur sur l'hôte
* faire en sorte que le conteneur soit build automatiquement si ce n'est pas fait

> On peut voir les logs d'un conteneurs qui tourne en démon avec `docker logs <ID_OR_NAME>`.

---

🌞 Ajouter un deuxième conteneur `docker-compose-v2.yml`
* ajouter un conteneur NGINX dans le `docker-compose-v2.yml`
* celui-ci doit agir comme reverse proxy vers votre serveur Python
  * il va falloir produire une configuration NGINX
    * la configuration doit être monté avec un volume au lancement du conteneur
    * si vous êtes pas à l'aise avec NGINX et sa config, cf le petit encart en dessous de cette liste
  * la connexion au conteneur NGINX doit se faire en HTTPS
    * le certificat et la clé pour la connexion doivent être générés avant le lancement du conteneur
    * ils sont montés avec un volume au lancement du conteneur
  * le port du conteneur NGINX doit être exposé sur l'hôte sur le port 443
  * le port du serveur web n'est plus exposé sur l'hôte
* [utiliser les `aliases` network](https://docs.docker.com/compose/compose-file/#aliases) pour que vos conteneurs communiquent entre eux

```
Client Web ---> VM ---> CT NGINX ---> CT Python
```

> Pour la configuration NGINX, si vous n'êtes pas trop familier, vous pourrez trouver un [exemple de configuration fonctionnelle dans le dépôt](./nginx/test.docker.conf). Cette configuration :  
> * suppose que vous avez déjà généré une paire de clé/certificats (avec pour nom `test.docker`)
> * suppose que votre clés et certificats sont dans `/certs`
> * suppose que votre app Python est joignable à l'adresse `python-app` sur le port `8888`
> * suppose que vous souhaitez joindre le service sur `http://test.docker`
> * le ficher de conf fourni est à déposer dans `/etc/nginx/conf.d/` sous le nom `test.docker.conf` 

## Rangement

A l'issue de cette étape, vous devez fournir une arborescence de fichiers fonctionnels avec :
* un Dockerfile (serveur web Python) 
* les `docker-compose.yml`
  * `docker-compose-v1.yml`
  * `docker-compose-v2.yml`
* une configuration NGINX
* des certificats pour NGINX

Je vous conseille l'arborescence suivante :
```
├── README.md
├── docker-compose-v1.yml
├── docker-compose-v2.yml
├── nginx
│   ├── certs
│   │   ├── nginx.crt
│   │   └── nginx.key
│   └── conf
│       └── nginx.conf
└── webserver
    └── Dockerfile
```

🌞 Le fichier `README.md` doit contenir des instructions simples sur le lancement de l'application avec `docker-compose`, et une explication succinte des différents composants

## Conteneuriser une application donnée

Ici on se rapproche d'un cas d'utilisation réel : je vous mets une application sur les bras et vous devez la conteneuriser. 

L'application : 
* codée en `python3`
  * [les sources sont dans ici](./python-app)
  * n'hésitez pas à cloner mon repo pour copier directement les fichiers
* nécessite des librairies installables avec `pip`
  * `pip install -r <FICHIER>`
* a besoin d'un Redis pour fonctionner
  * il doit être joignable sur le nom `db` (port par défaut (6379/TCP))

Vous devez :
* construire une image qui
  * contient `python3`
  * contient l'application et ses dépendances
  * lance l'application au démarrage du conteneur
* écrire un `docker-compose.yml`
  * contient l'application
  * contient un Redis
    * utilise l'image de *library*
    * a un alias `db`
  * contient un NGINX
    * reverse proxy HTTPS vers l'application Web
    * a son port 443 exposé

Structure attendue :
```
├── README.md
├── docker-compose.yml
├── nginx
│   ├── certs
│   │   ├── test.docker.crt
│   │   └── test.docker.key
│   └── conf.d
│       └── test.docker.conf
└── webserver
    ├── app
    │   ├── app.py
    │   ├── requirements
    │   └── templates
    └── Dockerfile
```
