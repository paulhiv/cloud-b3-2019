# Containers in-depth

- [Containers in-depth](#containers-in-depth)
- [I. Docker &amp; Kernel](#i-docker-amp-kernel)
  - [1. Gestion de conteneurs par Docker](#1-gestion-de-conteneurs-par-docker)
  - [2. Mécanismes utilisés par Docker](#2-m%c3%a9canismes-utilis%c3%a9s-par-docker)
    - [A. Namespaces](#a-namespaces)
      - [Isolation réseau ?](#isolation-r%c3%a9seau)
    - [B. Cgroups](#b-cgroups)
    - [C. Capabilities](#c-capabilities)
- [II. Standardisation](#ii-standardisation)

# I. Docker & Kernel

## 1. Gestion de conteneurs par Docker

Le démon Docker est nommé `dockerd`. C'est lui qui expose l'API avec laquelle on discute pour lancer des conteneurs. C'est à dire c'est lui qui reçoit les ordres `docker run` par exemple.  
Ce n'est cependant pas réellement lui qui gère la conteneurisation sur le système.  

Depuis quelques versions, c'est le démon `containerd` qui est chargé d'entretenir la vie des conteneurs. `containerd` est un *runtime de conteneurs*, c'est lui qui s'occupe notamment de lancer, surveiller, stopper, des conteneurs, *via* des API bas-niveau. C'est aussi lui qui gère les réseaux liés à Docker, le stockage, les images, et d'autres fonctionnalités (comme la gestion de logs, la récolte de métriques, etc.).  
`containerd` utilise `runc` pour lancer effectivement un conteneur.

`runc` est l'outil standard pour créer un environnement isolé dans le système dans lequel lancer un processus. En somme, un conteneur.  
`runc` va créer/allouer les ressources nécessaires au fonctionnement du conteneur (namespaces, cgroups, montage de volume, partage de ports, etc) ; puis `runc` aura la tâche de l'exécution du processus défini dans l'image choisie, dans cet environnement isolé.  
Une fois le conteneur créé, `runc` finit de s'exécuter.

Pour chaque conteneur, un *shim* (processus `containerd-shim`) sera présent. Ce *shim* permet de maintenir la communication entre `containerd` et le conteneur lancé dans un environnement isolé. Il permet notamment de récupérer le code retour du conteneur.

---

Ainsi, on peut décomposer les étapes liées à la commande suivante : 

```
$ docker run -d alpine sleep 9999
```

1. L'utilisateur, à l'aide de la commande `docker` envoie un message sur le socket dédiée à la communication avec `dockerd` (par défaut `/var/run/docker.sock`).  
Ce message est au format HTTP, l'API de Docker étant une API HTTP respectant les standards dans le monde de la conteneurisation.
2. Le démon `dockerd` reçoit la requête de l'utilisateur. Il fait appel à `containerd` pour lui demander de lancer le conteneur.
3. `containerd` reçoit la requête de `dockerd`, il prépare les ressources liées à cette requête.  
Par exemple, l'image demandée, les dossiers à monter dans le conteneurs, etc.
Enfin, il appelle `runc` pour créer le conteneur
4. `runc` crée l'environnement d'exécution du conteneur : les namespaces, cgroups, et autres éléments nécessaires à son fonctionnement.  
Il lance le processus dans le conteneur.  
5. Un processus `containerd-shim` est créé afin de maitenir la communication avec `containerd`
6. Le processus est correctement exécuté dans un environnement isolé.  

```
user --> dockerd --> containerd --> runc --> conteneur
```


## 2. Mécanismes utilisés par Docker

### A. Namespaces

Les *namespaces* constituent une fonctionnalité du noyau Linux. Ce sont des espaces où vivent les processus. **L'utilisation de namespaces permet d'isoler certaines partie du système pour un ou plusieurs processus donné(s).**  
* isolation des ressources système (ressources OS, PAS les ressources matérielles)
* les namespaces ont une structure arborescente
  * lorsque vous démarrez votre système, vous êtes dans les namespaces "principaux" ou "parents"
  * si vous créez de nouveaux namespaces, étant dans le namespace parent, vous voyez tout
  * mais les namespaces enfants ne voient pas ce qu'il y au-dessus d'eux
* on peut créer un processus dans un nouveau namespace donné avec l'appel système `unshare()` (la commande `unshare` permet de l'utiliser depuis la ligne de commande)

**Il existe 7 types de namespaces**
* **`mnt` pour "mount"**
  * isole l'arbre de montage
  * output différent avec `df -h` ou `mount`
* **`pid` pour "process ID"**
  * isole l'arbre de processus
  * output différent avec un `ps -ef`
* **`ipc` pour "inter-process communication"**
  * isole certains canaux de communication inter-processus
  * comme les bus D-Bus
* **`net` pour "network"**
  * isole la pile réseau
  * le nouveau processus n'a qu'une interface loopback (sauf configuré explicitement autrement)
  * output de `ip a` différent
* **`uts`**
  * isolation de la gestion des noms de domaine
  * output de `hostname` différent
* **`user`**
  * isolation de l'arbre utilisateur
  * pour que les utilisateurs des nouveaux namespaces puissent être visibles et exister dans le namespace parent, un système de mapping est mis en place
  * `root` dans un namespace enfant correspond à un utilisateur non-privilégié dans le namespace parent
* **`cgroup`**
  * arborescence de cgroups différentes
  * `/sys/fs/cgroup` différent

Par exemple, si deux processus (disons un terminal et un firefox) sont dans deux namespaces réseau différents, ils n'auront pas accès aux mêmes interfaces réseau.

---

**Dans le cas de Docker, chaque conteneur se voit attribuer un lot de namespaces qui lui est propre. Il est ainsi complètement isolé du système.**  
Le seul namespace dont l'activation est optionnelle est le namespace de type *user*.

---

#### Isolation réseau ? 

Ok mais si notre conteneur est totalement isolé, comment est-ce qu'il est possible de partager un port réseau de l'hôte vers un port réseau du conteneur ?  

Si notre conteneur possède ses propres cartes réseau et que l'hôte n'y a pas accès, comment un partage de port est-il possible ?  

Cela fait appel à deux outils que nous fournissent les systèmes GNU/Linux : 
* *veth pair*
* *iptables*

Par défaut, la plupart du réseau Docker sera géré avec *iptables*, l'outil de filtrage réseau par défaut des systèmes GNU/Linux. *iptables* est notamment l'outil utilisé pour mettre en place un firewall sur des OS GNU/Linux. 

> La commande `sudo iptables -vnL` permet d'avoir un aperçu de l'ensemble des règles *iptables*.  
Si vous utilisez Docker sur la machine, et que des conteneurs sont créés, beaucoup de règles relatives à Docker devraient exister.

La deuxième technologie utilisée porte le nom de *veth pair* ou "paire d'interface Ethernet virtuelle". Grâce à cette fonctionnalité, il est possible de créer deux cartes réseau qui sont intrinsèquement liées, et les deux pourront communiquer de façon directe. Elles sont comment les deux extrémités d'un tuyau : ce qui rentre dans une interface de la *veth pair* ressort par l'autre.  

---

Appliqué à Docker, et à notre connaissance des *namespaces*, on peut donc expliquer le fonctionnement du réseau natif de Docker.  
Prenons l'exemple de la création d'un conteneur NGINX qui partage son port 80 : 

```
$ docker run -d -p 8888:80 nginx
```

Dans ce cas, les opérations réalisées par Docker sont les suivantes : 
* création d'un nouveau namespace réseau dédié pour le conteneur
* création d'une *veth pair*
  * c'est bien une **paire** d'interfaces, donc 2 interfaces sont créées
  * l'une est placée sur l'hôte (namespace réseau parent)
  * la deuxième interface est placée dans le conteneur (namespace réseau enfant)
  * ainsi tout ce qui rentrera du côté de l'hôte sur l'interface dédiée, resortira dans le conteneur
* création d'une règle *iptables* pour permettre le forwarding du port 8888 de l'hôte vers le port 80 du conteneur

```
Client ===> Carte réseau host ==iptables==> Carte veth hôte ==veth pair==> Carte veth container
```

### B. Cgroups

Le terme *cgroup* désigne un mécanisme kernel de labellisation de processus et restriction de ressources appliquées aux processus.  

En clair, cela permet de rassembler les processus en groupe de processus. Pour ensuite potentiellement appliquer des restrictions d'accès aux ressources de la machines comme : 
* utilisation CPU
* utilisation RAM
* I/O disque
* utilisation réseau
* utilisation namespaces (later)

L'utilisation généralisée des cgroups au sein des systèmes GNU/Linux permet un plus grand contrôle sur les processus et une administration plus aisée.  
D'autres fonctionnalités qui en découlent sont le monitoring des processus par cgroups ou la priorisation de l'accès au ressources de la machines. 

> Cela permet par exemple une attribution plus fine de ressources qu'avec des mécanismes comme le *oom_score* (pour l'utilisation de la RAM) ou le *nice_score* (pour l'utilisation du CPU). Google it if ou don't know it.

**Les cgroups possèdent une structures arborescentes, comme une arborescende fichiers. Mais au lieu de ranger des fichiers, on range des processus.** 🔥🔥🔥

### C. Capabilities

Les capabilities correspondent à l'ensemble des superpouvoirs de `root` découpés en éléments atomiques. Entre autres :
* `CAP_DAC_OVERRIDE` : permet d'outrepasser les droits POSIX (rwx et proriétaire)
* `CAP_SYS_CHROOT` : permet d'utiliser `chroot`  et d'avoir le contrôle sur les namespaces de type mount
* `CAP_SYS_TIME` : permet de définir l'heure du système et l'heure hardware (RTC)

> Vous pouvez trouver une liste complète des capabilities Linux [ici par exemple](http://man7.org/linux/man-pages/man7/capabilities.7.html).

La gestion des capabilities est complexe (chaque processus a 5 sets différents de capabilities). Le set "Effective" contient les capabilities utilisables par un processus donné.  
C'est à ce set que l'on fait référence lorsque l'on regarde les capabilities utilisées par un processus.

Lorsqu'un processus est lancé, il est toujours lancé par un autre processus. Ainsi, il est l'enfant du processus parent.  
**Lorsqu'il est créé, le processus enfant ne peut PAS hériter de toutes les capabilites, mais uniquement celles que son parent autorise.**

# II. Standardisation

La standardisation des outils de conteneurisation est visible à plusieurs niveaux :
* standardisation de l'API du runtime de conteneurisation
  * l'API de docker sert souvent de référence
* [standardisation du runtime](https://github.com/opencontainers/runtime-spec)
  * initiative portée par l'OCI
* [standardisation du format de l'image](https://github.com/opencontainers/image-spec)
  * initiative portée par l'OCI

Cette standardisation permet une totale intéropérabilité des outils, et permettent une le support de ce qui a déjà été construit au fil du temps. Cela permet aussi à de nouveaux outils de voir le jour plus rapidement, en exploitant ce travail qui a été fait au paravant.  

> Grâce à cette standardisation, il est possible par exemple, de créer une image avec Docker, et de l'exécuter avec rkt (un autre outil de conteneurisation).  

C'est aussi grâce à la standardisation de l'API qu'il est possible d'utiliser autre chose que `runc` ou `containerd` derrière l'API que peut nous présenter le démon Docker, et donc sans changer les commandes `docker` que l'on a l'habitude d'utiliser.

```
Client standard ---> API standard ---> Runtime standard ---> Image standard

# Par exemple
docker ---> dockerd ---> containerd + runc ---> Image docker

docker ---> dockerd ---> kata container ---> Image ACI
```
